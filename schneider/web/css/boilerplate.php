<?php ?>
/************************************************************************/
/*																		*/
/* Client Specific Style Overrides										*/
/* Client: SD Edge													*/
/* July 03th, 2015														*/
/*																		*/
/************************************************************************/

/* General Overrides */

/* Body Background */
/* Change this if there is going to be a background image on the body*/
/* Question 4 value */
body { 
	background-color:<?=  $data["question-15-a"] ?>;  /* Replace with the hexcode for the background color */
	background-image:url(./Art/en/<?=  $data["question-15-b"] ?>); /* If there is a background image provided, replace with the file path */
	background-repeat: <?=  $data["question-15-c"] ?>; /* Replace with proper repeat syntax, possible options are: repeat, no-repeat, repat-y, repeat-x;  */
	background-position: <?=  $data["question-15-d"] ?> <?=  $data["question-15-e"] ?>;
}


/* Links */
/* Chnage this to the primary branding color */
/* Question 5 value */
a,
a:link,
a:visited,
a:link,
a:hover,
a span,
#login a,
#selaccts.accounts > table:nth-child(2) table td a,
#curtailment-event-details #graph-summary table td a.details-link,
#selaccts.groups > div.container > table.list table td a,
#divButtons li a,
#linksForPrinting li a,
a[class*="ui-date"] span:after  { color:<?=  $data["question-5"] ?>;  } /* data-id="question-5" */

/* Buttons: Question 6 Value */
input.button,
a.button,
#selaccts a.button,
#divChartOptions input[type=button],
#customer-selection .multi-select a.button,
#login a.button,
#login input.button,
#reports-events > form > ul.advanced-filter li:last-child input,
#curtailment-event-details input[type=button],
aside[id*="edit-"] .button, 
#account-attributes .button  {
	border:1px solid <?=  $data["question-6-b-b"] ?>; /* data-id="question-6-b-b" */
	background-color:<?=  $data["question-6-a"] ?>; /* data-id="question-6-a" */
	background:-webkit-gradient(linear, top, from(<?=  $data["question-6-b-a"] ?>) 0%, to(<?=  $data["question-6-b-b"] ?>) 100%);/* data-id="question-6-b-a" */  /* data-id="question-6-b-b" */
	background-image:-webkit-linear-gradient( top, <?=  $data["question-6-b-a"] ?> 0%, <?=  $data["question-6-b-b"] ?> 100%); /* data-id="question-6-b-a" */  /* data-id="question-6-b-b" */
	background-image:-moz-linear-gradient( top, <?=  $data["question-6-b-a"] ?> 0%, <?=  $data["question-6-b-b"] ?> 100%); /* data-id="question-6-b-a" */  /* data-id="question-6-b-b" */
	background-image:-ms-linear-gradient( top, <?=  $data["question-6-b-a"] ?> 0%, <?=  $data["question-6-b-b"] ?> 100%); /* data-id="question-6-b-a" */  /* data-id="question-6-b-b" */
	-ms-filter:"progid:DXImageTransform.Microsoft.gradient (GradientType=0, startColorstr=<?=  $data["question-6-b-a"] ?>, endColorstr=<?=  $data["question-6-b-b"] ?>)"; /* data-id="question-6-b-a" */  /* data-id="question-6-b-b" */
	color:<?=  $data["question-6-c"] ?>;
}

/* UI Highlights: Question 7 Value - Convert Hex to RGB, Change values in rgba() without changing last value (0.80) */
#divChartOptions .multi-select .container .ms-parent:hover,
#divChartOptions .select .styled-select:hover {
	-webkit-box-shadow:0 0 4px 1px rgba(<?=  $data["question-7"] ?>, 0.8); /* data-id="question-7" */ 
}

/* Header Region Overrides */

/* Header: Question 2 Value */
header {
	background:none;
	background-color:<?=  $data["question-2-a"] ?>; /* data-id="question-2-a" */
}
/* Header Logo: These elements can be resized to work with various sized logos from clients */
header > section {
	background:none;
	padding-left:0;
}
/* Logo: Question 1 Value */ header > section h1 {
	display:inline-block;
	float:left;
	margin-top:0;
	padding:0;
	width:950px;
	height:53px;
	overflow:hidden;
	text-indent:-9999px;
	font-size:1px;
	background:url(./Art/logo/<?=  $data["question-1"] ?>) no-repeat 0 0; /* data-id="question-1" just the file path*/
	background-size: auto 100%;
}
/* Header: User Name Link: Question 3 Value */
header > section > ul > li:first-child > a { color:<?=  $data["question-3"] ?> !important; } /* data-id="question-3" */


/* Main Navigation */
/* Navigation Background Gradient: Question 8 Value; */
nav {
	background-color:#b4b4b4; /* data-id="question-8-a" */
	background:-webkit-gradient(linear, bottom, from(<?=  $data["question-8-b-b"] ?>), to(<?=  $data["question-8-b-a"] ?>) 100%); /* data-id="question-8-b-b" */ /* data-id="question-8-b-a" */
	background:-webkit-linear-gradient(bottom, <?=  $data["question-8-b-b"] ?>, <?=  $data["question-8-b-a"] ?> 100%); /* data-id="question-8-b-b" */ /* data-id="question-8-b-a" */
	background:-moz-linear-gradient(bottom, <?=  $data["question-8-b-b"] ?>, <?=  $data["question-8-b-a"] ?> 100%); /* data-id="question-8-b-b" */ /* data-id="question-8-b-a" */
	background:-o-linear-gradient(bottom, <?=  $data["question-8-b-b"] ?>, <?=  $data["question-8-b-a"] ?> 100%); /* data-id="question-8-b-b" */ /* data-id="question-8-b-a" */
	background:linear-gradient(bottom, <?=  $data["question-8-b-b"] ?>, <?=  $data["question-8-b-a"] ?> 100%); /* data-id="question-8-b-b" */ /* data-id="question-8-b-a" */
	-ms-filter:"progid:DXImageTransform.Microsoft.gradient (GradientType=0, startColorstr=<?=  $data["question-8-b-a"] ?>, endColorstr=<?=  $data["question-8-b-b"] ?>)"; /* data-id="question-8-b-b" */ /* data-id="question-8-b-a" */
	

}
/* Tab Colors: Question 9 Value */
nav > ul > li { background:<?=  $data["question-9-a"] ?> !important; } /* data-id="question-9-a" */
nav > ul > li:hover { background:<?=  $data["question-9-b"] ?> !important; } /* data-id="question-9-b" */
nav > ul > li.active { background:<?=  $data["question-9-c"] ?> !important; } /* data-id="question-9-c" */

/* Tab Text Colors: Change these to be the most legible with the background color - white or dark gray */
nav > ul > li a {color: <?=  $data["question-16-a"] ?> !important; }
nav > ul > li:hover a {color: <?=  $data["question-16-b"] ?> !important; }
nav > ul > li.active a {color: <?=  $data["question-16-c"] ?> !important; }


/* Content Region Overrides */

/* Titles: Question 12 Value */
#login-page section#introduction h2,
#customer-selection .multi-select h2,
#content aside h2,
#preferences > table > tbody > tr > td > b,
label#lblText {  color:<?=  $data["question-12"] ?>; } /* data-id="question-12" */


/* Left Navigation & Containers: Question 11 Value - Change color of file found in Art/en/bg-tile-blocktop-blue.gif */
#content aside.chart-wrapper,
#content aside#newemissionreport,
#content aside#newbillestimation,
#content aside#newgooglemap,
#content aside#reports-events,
#content aside#curtailment-event-details,
#content aside#group-edit-add,
aside#grid-container,
#content aside#account-attributes,
#divChartOptions,
#historicalContain .dateContain,
#profiles,
#content aside#ReportParameters { background-image:url(./Art/en/<?=  $data["question-11"] ?>) !important; } /* data-id="question-11" */

/* Left Navigation Menu: Question 10 Value */
#content aside#profiles { background-color:<?=  $data["question-10-a"] ?>; }

#content aside#profiles ul li,
#divChartOptions .accordion li .submenu li { background-color:<?=  $data["question-10-a"] ?>; } /* data-id="question-10-a" */

#content aside#profiles ul li a,
#divChartOptions .accordion li .submenu li a { color:<?=  $data["question-10-d"] ?>; }

/* Left Navigation Menu - Hover: Question 10 Value  */
#content aside#profiles ul li:hover,
#divChartOptions .accordion li .submenu li:hover { background-color:<?=  $data["question-10-c"] ?>; }
#content aside#profiles ul li:hover a,
#divChartOptions .accordion li .submenu li:hover a { color:<?=  $data["question-10-e"] ?>; }  /* data-id="question-10-c" */

/* Left Navigation Menu - Active: Question 10 ValuMe */
#content aside#profiles ul li.active,
#divChartOptions .accordion li .submenu li.active {
	background-color:<?=  $data["question-10-b"] ?>;
	color:<?=  $data["question-10-f"] ?>;
}
#content aside#profiles ul li.active a,
#divChartOptions .accordion li .submenu li.active a {
	color:<?=  $data["question-10-f"] ?>;
}
 /* data-id="question-10-b" just background color */
#content aside#profiles ul li.active:hover a,
#divChartOptions .accordion li .submenu li.active:hover a { color:#fff; }

/* Date Picker */

/* Today's date: Qustion 13 Value */
.date-picker-wrapper .month-wrapper table .day.real-today,
.date-picker-wrapper .month-wrapper table .day.real-today.checked,
.date-picker-wrapper .month-wrapper table .day.real-today:hover,
.date-picker-wrapper .month-wrapper table .day.real-today.checked:hover {
	background-color:<?=  $data["question-13-b"] ?>; /* data-id="question-13-b" just background-color */
	color:#fff !important;
}

/* Checked Days: Qustion 13 Value */
.date-picker-wrapper .month-wrapper table .day.checked { background-color:<?=  $data["question-13-a"] ?>; } /* data-id="question-13-a" */

/* Checked Days - Hover */
.date-picker-wrapper .month-wrapper table .day.toMonth.checked:hover,
.date-picker-wrapper .month-wrapper table .day.checked:hover { 
	background-color:<?=  $data["question-13-b"] ?>; /* data-id="question-13-b" just background-color */
	color:#fff;
}

/* Login Page */
#login-page section#introduction p { background-image:url(./Art/en/<?=  $data["question-14-a"] ?>); }
#login-page section#introduction,
#login div.login-footer { background:<?=  $data["question-14-b"] ?>; }


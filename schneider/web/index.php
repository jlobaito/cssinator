<?php

include "lib/cssinator.php";
error_reporting(1);

if(isset($_GET["client"])){

    $file = $_SERVER["DOCUMENT_ROOT"] . "/schneider/web/css/" . $_GET["client"] . ".json";
    $j = json_decode(file_get_contents($file), TRUE);
    
}

?><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Schneider Electric | EPO Rebrand</title>
    <meta name="description" content="[[*description]]">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <!-- THIS IS NEEDED FOR FURL -->

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <!-- Facebook OpenGraph Data -->
    <meta property="og:title" content="[[++site_name]]" />
    <meta property="og:type" content="website" />
        <!-- If it's an article:
        <meta property="og:type" content="article" />
        <meta property="article:published_time" content = "" />
    -->
    <!-- Canonical URL for CMS SEO -->
    <link rel="canonical" href="[[++site_url]]" />

    <link rel="stylesheet" href="resources/css/base.css"> 

        <!--[if lte IE 8]>
        <link rel="stylesheet" href="/resources/css/fallback.css">
        <link rel="stylesheet" href="/resources/css/ie8.css">
        <![endif]-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="resources/js/modernizr.js"></script>
        <script src="resources/js/respond.min.js"></script>
        <!-- FAVICON -->
        <link rel="icon" href="resources/img/favicon.png" type="image/png">      
    </head>
    <body>
        <header class="mainHeader">
            <div class="container wrap">
                <img src="resources/img/logo.png" alt="" class="logo">
                <h1>Schneider Electric: EPO Rebrand Tool</h1>
            </div>
        </header>
        <div class="container wrap">
            <div class="row">
                <div class="col-md-4">
                    <div class = " quickNav">
                        <h2>Quick Navigation</h2>
                        <ul class="styles">
                            <li><a href="#1">Client Name</a></li>
                            <li><a href="#15">Main body styling</a></li>
                            <li><a href="#2">Header logo image</a></li>
                            <li><a href="#3">Header background</a></li>
                            <li><a href="#4">Header link color</a></li>
                            <li><a href="#6">Link color</a></li>
                            <li><a href="#7">Button color/gradient</a></li>
                            <li><a href="#8">Interface highlight color - element outline</a></li>
                            <li><a href="#9">Navigation background color/gradient</a></li>
                            <li><a href="#10">Tab colors</a></li>
                            <li><a href="#11">Left navigation colors</a></li>
                            <li><a href="#12">Container Header Color</a></li>
                            <li><a href="#13">Title color</a></li>
                            <li><a href="#14">Date highlight colors</a></li>
                            <li><a href="#16">Login page</a></li>

                        </ul>
                        <h2>Archived Files</h2>
                        <div class="archives">
                            <ul>
                                <?
                                if(!empty($filelist)){
                                    foreach($filelist as $f){
                                        $path = str_replace($_SERVER["DOCUMENT_ROOT"] . "/schneider/web/", "", $f);
                                        $filename = str_replace("css/stylesheet-CUSTOM-", "", $path);
                                        $edit = str_replace(".css", "", $filename);
                                        ?>
                                        <li><span><?= $edit ?>:</span><br><a href = "<?= $path ?>" target = "_blank">download</a> | <a href = "index.php?client=<?= $edit ?>" target = "">edit</a></li>
                                        <?  }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div>
                        

                    </div>
                    <div class="col-md-8">
                        <form action="" method = "POST" >
                            <ul>
                                <li class="well" id="1">
                                    <h2>Client Name</h2>
                                    <input name="client-name" type="text" value = "<?= $j["client-name"] ? : "" ?>">

                                </li>
                                <li class="well" id="15">

                                    <h2>Main body styling</h2>
                                    <div class="color"
                                    <label for="">Body color hex value:</label>

                                            <div class="input-group ">
                                                <input type="text" value = "<?= $j["question-15-a"] ? : "#fff"  ?>" class="form-control" name="question-15-a"/>
                                                <span class="input-group-addon"><i></i></span>
                                            </div>
                                        </div>
                                    <h3>Or image:</h3>
                                    <label for="logo-file">File name:</label>
                                    <input type="text" value = "<?= $j["question-15-b"] ? : "" ?>" name="question-15-b">
                                    <small>This only changes the CSS. Image files still need to uploaded to the server. <strong>Upload to /Art/logo/</strong></small>

                                    <h3>If image is set, apply these values:</h3>

                                    <label for="logo-file">Background repeat:</label>
                                    <small>Possible values are: repeat, no-repeat, repeat-x, repeat-y</small>
                                    <input type="text" value = "<?= $j["question-15-c"] ? : "repeat"  ?>" name="question-15-c"><br><br>
                                    <label for="logo-file">Background position - X:</label>
                                    <small>Pixels or percentage, must append value with proper prefix (px, %)</small>
                                    <input type="text" value = "<?= $j["question-15-d"] ? : "0px"  ?>"  name="question-15-d"><br><br>
                                    <label for="logo-file">Background position - Y:</label>
                                    <small>Pixels or percentage, must append value with proper prefix (px, %)</small>
                                    <input type="text" value = "<?= $j["question-15-e"] ? : "0px"  ?>"  name="question-15-e">
                                </li>
                                <li class="well" id="2">

                                    <h2>Header logo image</h2>
                                    <label for="logo-file">File name:</label>
                                    <input type="text" value = "<?= $j["question-1"] ? : "" ?>" name="question-1">

                                    <small>This only changes the CSS. Image files still need to uploaded to the server. <strong>Upload to /Art/logo/</strong></small>

                                    <ul>
                                        <li>File types: .gif, .jpg, .png</li>
                                        <li>Dimensions: height = 62px</li>
                                        <li>width = up to 600px</li>
                                    </ul>

                                </li>
                                <li class="well" id="3">
                                    <h2>Header background</h2>
                                    <h3>Background Color</h3>
                                    <label for="">Color hex value:</label>
                                    <div class="input-group color">
                                        <input type="text" value="<?= !empty($j["question-2-a"]) ? $j["question-2-a"] : "#3e8e96" ?>" class="form-control" name="question-2-a" />
                                        <span class="input-group-addon"><i></i></span>
                                    </div>
                                    <br>
                                    <h3>Or gradient</h3>
                                    <div class="row">
                                        <div class="color col-md-6">
                                            <label for="">Top hex value:</label>

                                            <div class="input-group ">
                                                <input type="text" value = "<?= $j["question-2-b-a"] ? : "#3e8e96"  ?>" class="form-control" name="question-2-b-a"/>
                                                <span class="input-group-addon"><i></i></span>
                                            </div>
                                        </div>
                                        <div class="color col-md-6">
                                            <label for="">Bottom hex value:</label>

                                            <div class="input-group ">

                                                <input type="text" value = "<?= $j["question-2-b-b"] ? : "#3e8e96"  ?>" class="form-control" name="question-2-b-b"/>
                                                <span class="input-group-addon"><i></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <h3>Or image:</h3>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="">File name:</label>
                                            <input type="text" value = "<?= $j["question-2-c"] ? : "" ?>" name="question-2-c">
                                        </div>
                                    </div>
                                    <small>This only changes the CSS. Image files still need to uploaded to the server. <strong>Upload to /Art/en/</strong></small>

                                </li>
                                <li class="well" id="4">
                                    <h2>Header link color</h2>
                                    <div class="color">
                                        <label for="">Color hex value:</label>

                                        <div class="input-group ">

                                            <input type="text" value = "<?= $j["question-3"] ? : "#3e8e96"  ?>" class="form-control" name="question-3"/>
                                            <span class="input-group-addon"><i></i></span>
                                        </div>
                                    </div>
                                </li>
                                <li class="well" id="6">
                                    <h2>Link color</h2>
                                    <label for="">Color hex value:</label>
                                    <div class="color">
                                        <div class="input-group ">

                                            <input type="text" value = "<?= $j["question-5"] ? : "#3e8e96"  ?>" class="form-control" name="question-5"/>
                                            <span class="input-group-addon"><i></i></span>
                                        </div>
                                    </div>
                                </li>
                                <li class="well" id="7">
                                    <h2>Button color/gradient</h2>
                                    <h3>Background color</h3>
                                    <label for="color">Color hex value:</label>
                                    <div class="input-group color">
                                        <input type="text" value = "<?= $j["question-6-a"] ? : "#3e8e96"  ?>" class="form-control" name="question-6-a"/>
                                        <span class="input-group-addon"><i></i></span>
                                    </div>
                                    <br>
                                    <h3>Or gradient</h3>
                                    <div class="row">
                                        <div class="color col-md-6">
                                            <label for="">Top hex value:</label>

                                            <div class="input-group ">
                                                <input type="text" value = "<?= $j["question-6-b-a"] ? : "#3e8e96"  ?>" class="form-control" name="question-6-b-a"/>
                                                <span class="input-group-addon"><i></i></span>
                                            </div>
                                        </div>
                                        <div class="color col-md-6">
                                            <label for="">Bottom hex value:</label>

                                            <div class="input-group ">

                                                <input type="text" value = "<?= $j["question-6-b-b"] ? : "#3e8e96"  ?>" class="form-control" name="question-6-b-b"/>
                                                <span class="input-group-addon"><i></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <h3>Font color</h3>
                                    <label for="color">Color hex value:</label>
                                    <div class="input-group color">
                                        <input type="text" value = "<?= $j["question-6-c"] ? : "#3e8e96"  ?>" class="form-control" name="question-6-c"/>
                                        <span class="input-group-addon"><i></i></span>
                                    </div>
                                </li>
                                <li class="well" id="8">
                                    <h2>Interface highlight color - element outline</h2>
                                    <label for="color">Color hex value:</label>
                                    <div class="input-group color">
                                        <input type="text" data-format="rgba" value = "<?= $j["question-7"] ? : "rgba(10,193,212,0.43)"  ?>" class="form-control" name="question-7"/>
                                        <span class="input-group-addon"><i></i></span>
                                    </div>
                                </li>
                                <li class="well" id="9">
                                    <h2>Navigation background color/gradient</h2>
                                    <h3>Background color</h3>
                                    <label for="color">Color hex value:</label>
                                    <div class="input-group color">
                                        <input type="text" value = "<?= $j["question-8-a"] ? : "#3e8e96"  ?>" class="form-control" name="question-8-a"/>
                                        <span class="input-group-addon"><i></i></span>
                                    </div>
                                    <br>
                                    <h3>Or gradient</h3>
                                    <div class="row">
                                        <div class="color col-md-6">
                                            <label for="">Top hex value:</label>

                                            <div class="input-group ">
                                                <input type="text" value = "<?= $j["question-8-b-a"] ? : "#3e8e96"  ?>" class="form-control" name="question-8-b-a" />
                                                <span class="input-group-addon"><i></i></span>
                                            </div>
                                        </div>
                                        <div class="color col-md-6">
                                            <label for="">Bottom hex value:</label>

                                            <div class="input-group ">

                                                <input type="text" name="question-8-b-b" value = "<?= $j["question-8-b-b"] ? : "#3e8e96"  ?>" class="form-control" />
                                                <span class="input-group-addon"><i></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="well" id="10">
                                    <h2>Tab colors</h2>
                                    <label for="">Inactive/standard hex value:</label>
                                    <div class="color">
                                        <div class="input-group ">

                                            <input type="text" value = "<?= $j["question-9-a"] ? : "#3e8e96"  ?>" class="form-control" name="question-9-a"/>
                                            <span class="input-group-addon"><i></i></span>
                                        </div>
                                    </div>
                                    <br>
                                    <label for="">Hover/Mouseover hex value:</label>
                                    <div class="color">
                                        <div class="input-group ">

                                            <input type="text" value = "<?= $j["question-9-b"] ? : "#3e8e96"  ?>" class="form-control" name="question-9-b"/>
                                            <span class="input-group-addon"><i></i></span>
                                        </div>
                                    </div>

                                    <br>
                                    <label for="">Active/standard hex value: </label>
                                    <div class="color">
                                        <div class="input-group ">

                                            <input type="text" value = "<?= $j["question-9-c"] ? : "#3e8e96"  ?>" class="form-control" name="question-9-c"/>
                                            <span class="input-group-addon"><i></i></span>
                                        </div>
                                    </div>
                                    <br>
                                    <h2>Tab text colors</h2>
                                    <small>Generally these will remain white</small>
                                    <label for="">Inactive/standard hex value:</label>
                                    <div class="color">
                                        <div class="input-group ">

                                            <input type="text" value = "<?= $j["question-16-a"] ? : "#fff"  ?>" class="form-control" name="question-16-a"/>
                                            <span class="input-group-addon"><i></i></span>
                                        </div>
                                    </div>
                                    <br>
                                    <label for="">Active/standard hex value:</label>
                                    <div class="color">
                                        <div class="input-group ">

                                            <input type="text" value = "<?= $j["question-16-b"] ? : "#fff"  ?>" class="form-control" name="question-16-b"/>
                                            <span class="input-group-addon"><i></i></span>
                                        </div>
                                    </div>

                                    <br>
                                    <label for="">Hover/Mouseover hex value:</label>
                                    <div class="color">
                                        <div class="input-group ">

                                            <input type="text" value = "<?= $j["question-16-c"] ? : "#fff"  ?>" class="form-control" name="question-16-c"/>
                                            <span class="input-group-addon"><i></i></span>
                                        </div>
                                    </div>
                                </li>

                                <li class="well" id="11">
                                    <h2>Left navigation background colors</h2>
                                    <label for="">Inactive/standard hex value:</label>
                                    <div class="color">
                                        <div class="input-group ">

                                            <input type="text" value = "<?= $j["question-10-a"] ? : "#3e8e96"  ?>" class="form-control" name="question-10-a"/>
                                            <span class="input-group-addon"><i></i></span>
                                        </div>
                                    </div>
                                    <br>
                                    <label for="">Active/standard hex value:</label>
                                    <div class="color">
                                        <div class="input-group ">

                                            <input type="text" value = "<?= $j["question-10-b"] ? : "#3e8e96"  ?>" class="form-control" name="question-10-b"/>
                                            <span class="input-group-addon"><i></i></span>
                                        </div>
                                    </div>

                                    <br>
                                    <label for="">Hover/Mouseover hex value:</label>
                                    <div class="color">
                                        <div class="input-group ">

                                            <input type="text" value = "<?= $j["question-10-c"] ? : "#3e8e96"  ?>" class="form-control" name="question-10-c"/>
                                            <span class="input-group-addon"><i></i></span>
                                        </div>
                                    </div>
                                    <br>
                                    <h2>Left navigation text colors</h2>
                                    <label for="">Inactive/standard hex value:</label>
                                    <div class="color">
                                        <div class="input-group ">

                                            <input type="text" value = "<?= $j["question-10-d"] ? : "#3e8e96"  ?>" class="form-control" name="question-10-d"/>
                                            <span class="input-group-addon"><i></i></span>
                                        </div>
                                    </div>
                                    <br>
                                    <label for="">Active/standard hex value:</label>
                                    <div class="color">
                                        <div class="input-group ">

                                            <input type="text" value = "<?= $j["question-10-f"] ? : "#fff"  ?>" class="form-control" name="question-10-f"/>
                                            <span class="input-group-addon"><i></i></span>
                                        </div>
                                    </div>

                                    <br>

                                    <label for="">Hover/Mouseover hex value:</label>
                                    <div class="color">
                                        <div class="input-group ">

                                            <input type="text" value = "<?= $j["question-10-e"] ? : "#3e8e96"  ?>" class="form-control" name="question-10-e"/>
                                            <span class="input-group-addon"><i></i></span>
                                        </div>
                                    </div>
                                </li>

                                <li class="well" id="12">
                                    <h2>Container Header Color</h2>
                                    <label for="">File name:</label>
                                    <input type="text" value = "<?= $j["question-11"] ? : ""  ?>" name="question-11">
                                    <br><small>This only changes the CSS. Image files still need to uploaded to the server. <strong>Upload to /Art/en/</strong></small>
                                </li>

                                <li class="well" id="1">
                                    <h2>Title color</h2>
                                    <label for="">Color hex value:</label>
                                    <div class="color">
                                        <div class="input-group ">

                                            <input type="text" value = "<?= $j["question-12"] ? : "#3e8e96"  ?>" class="form-control" name="question-12"/>
                                            <span class="input-group-addon"><i></i></span>
                                        </div>
                                    </div>
                                </li>

                                <li class="well" id="13">
                                    <h2>Date highlight colors</h2>
                                    <label for="">Highlight hex value:</label>
                                    <div class="color">
                                        <div class="input-group ">

                                            <input type="text" value = "<?= $j["question-13-a"] ? : "#3e8e96"  ?>" class="form-control" name="question-13-a"/>
                                            <span class="input-group-addon"><i></i></span>
                                        </div>
                                    </div>
                                    <br>
                                    <label for="">Focus/Today hex value:</label>
                                    <div class="color">
                                        <div class="input-group ">

                                            <input type="text" value = "<?= $j["question-13-b"] ? : "#3e8e96"  ?>" class="form-control" name="question-13-b"/>
                                            <span class="input-group-addon"><i></i></span>
                                        </div>
                                    </div>
                                </li>
                                <li class="well" id="16">
                                    <h2>Login Page</h2>
                                    <label for="">Computer display preview file name:</label>
                                    <input type="text" value = "<?= $j["question-14-a"] ? : ""  ?>" name="question-14-a">
                                    <br><small>This image needs to be created. This only changes the CSS. Image files still need to uploaded to the server. <strong>Upload to /Art/en/</strong></small>
                                    <br>
                                    <label for="">Login screen footer color:</label>
                                    <div class="color">
                                        <div class="input-group ">

                                            <input type="text" value = "<?= $j["question-14-b"] ? : "#3e8e96"  ?>" class="form-control" name="question-14-b"/>
                                            <span class="input-group-addon"><i></i></span>
                                        </div>
                                    </div>
                                </li>
                                <li class="well" id="14">
                                    <h2>Create CSS file</h2>
                                    <label for=""></label>
                                    <div>
                                        <div class="input-group ">
                                            <button type = "submit">Create file</button>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </form>

            </div><!-- 
            <div class="col-md-2">
                <h2>Archived Styles:</h2>
            </div> -->
            
        </div>

    </div>



    <script src="resources/js/production.min.js"></script>
    <script>
        $(function(){
            $('.color').colorpicker();
        });
    </script>
</body>
</html>

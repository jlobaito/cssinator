<?php 

include "lib/cssinator.php";
error_reporting(1);

if(isset($_GET["client"])){

	$file = $_SERVER["DOCUMENT_ROOT"] . "/cssinator/css/" . $_GET["client"] . ".json";
	$json = json_decode(file_get_contents($file), TRUE);
	$css = $json["css"];
	$filename = $json["filename"];
	
}

//print_r($client);
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Bootstrap 101 Template</title>

	<!-- Bootstrap -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">


	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>
	<body>
		<div class = "container">
			<h1>CSS-inator</h1>

			<form class="form-inline" method = "POST">
				<div class = "well">
					<h3>.class-one</h3>
					<div class="form-group">
						<label for="class-one-background">background</label>
						<input type="text" name = "css[class-one][background]" class="form-control" id="class-one-background" placeholder="#333333" value = "<?= $css["class-one"]["background"] ?>">
					</div>
					<div class="form-group">
						<label for="class-one-color">color</label>
						<input type="text" name = "css[class-one][color]" class="form-control" id="class-one-color" placeholder="#ffffff" value = "<?= $css["class-one"]["color"] ?>">
					</div>
				</div>
				<div class = "well">
					<h3>.class-two</h3>
					<div class="form-group">
						<label for="class-two-background">background</label>
						<input type="text" name = "css[class-two][background]" class="form-control" id="class-two-background" placeholder="#333333" value = "<?= $css["class-two"]["background"] ?>">
					</div>
					<div class="form-group">
						<label for="class-two-color">color</label>
						<input type="text" name = "css[class-two][color]" class="form-control" id="class-two-color" placeholder="#ffffff" value = "<?= $css["class-two"]["color"] ?>">
					</div>
				</div>
				<div class = "well">
					<div class="form-group">
						<label for="filename">Filename</label>
						<input type="text" name = "filename" class="form-control" id="filename" placeholder="client-name" value = "<?= $filename ?>">
					</div>
					<button type="submit" class="btn btn-default">CSS-inator-rize it!!!</button>
				</div>
			</form>
			
			<div class = "well">
				<h2>Existing files</h2>
				<ul>
					<?
					if(!empty($filelist)){
						foreach($filelist as $f){
							echo $f;
							$path = str_replace($_SERVER["DOCUMENT_ROOT"] . "/cssinator/", "", $f);
							$filename = str_replace("css/stylesheet-CUSTOM-", "", $path);
							$edit = str_replace(".css", "", $filename);
							?>
							<li><?= $filename ?> - <a href = "<?= $path ?>" target = "_blank">download</a> | <a href = "index.php?client=<?= $edit ?>" target = "">edit</a></li>
							<?	}
						}
						?>
					</ul>
				</div>
			</div>
			<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
			<!-- Include all compiled plugins (below), or include individual files as needed -->
			<!-- Latest compiled and minified JavaScript -->
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		</body>
		</html>
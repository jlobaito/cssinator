module.exports = function(grunt) {

    // 1. All configuration goes here 
    grunt.initConfig({
    	pkg: grunt.file.readJSON('package.json'),

    	concat: {
    		js: {
    			src: [
            'resources/js/*.js', // All JS in the libs folder
            'resources/js/libs/*.js',
            'resources/js/vendor/*.js'
            ],
            dest: '../schneider/web/resources/js/production.js',
        },
            css: {
                src: [
            'resources/css/*.css'
            ],
            dest: '../schneider/web/resources/css/production.css',
        },

            
    },
    uglify: {
    	js: {
    		src: '../schneider/web/resources/js/production.js',
    		dest: '../schneider/web/resources/js/production.min.js'
    	}
    },
    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: '../schneider/web/resources/img/',
          src: ['*.{png,jpg,gif}'],
          dest: '../schneider/web/resources/img/'
        }]
      }
    },
    sass: {
    	dist: {
    		src: [
                'resources/sass/base.scss'
            ],
            dest: '../schneider/web/resources/css/base.css',
    		
    	} 
    },
    watch: {
    	options: {
    		livereload: true,
    	},
    	scripts: {
    		files: [
    		'resources/js/*.js', 
    		'resources/js/libs/*.js',
    		'resources/js/vendor/*.js'
    		],
    		tasks: ['concat', 'uglify'],
    		options: {
    			spawn: false,
    		},
    	} ,
    	css: {
    		files: ['resources/sass/*.scss'],
    		tasks: ['sass'],
    		options: {
    			spawn: false,
    		}
    	},
        
          html: {
            files: ['../schneider/web/*.html','../schneider/web/resources/css/*.css'],
            options: {
                livereload: true
            }
        },
        images: {
            files: ['../schneider/web/resources/img/*.{png,jpg,gif}', '../schneider/web/resources/img/*.{png,jpg,gif}'],
            tasks: ['imagemin'],
            options: {
              spawn: false,
            }
          }
    }

});

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-sass');
    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['concat', 'uglify', 'imagemin', 'sass', 'watch']);

};